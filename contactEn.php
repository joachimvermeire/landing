<?php

if (isset($_POST['submit'])) {

  $name = $_POST['name'];
  $company = $_POST['company'];
  $mailFrom = $_POST['email'];
  $tel = $_POST['phone'];
  $country = $_POST['country'];
  $message = $_POST['message'];
  $subject = "I want additional info";
  $mailTo = "contact@craftyourtaste.com";
  $method = $_POST['radio-grp'];

  $captcha=$_POST['g-recaptcha-response'];

  if(!$captcha){
    echo '<h2>Please check the the captcha form.</h2>';
    exit;
  }

  $secretKey = "6LeocbQaAAAAANqvchsF9MiD0vyO9rEJWn6eP3Ug";
        $ip = $_SERVER['REMOTE_ADDR'];
        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        // should return JSON with success as true
        if($responseKeys["success"]) {
          $Body = "Nieuwe email van: ";
          $Body .= $name;
          $Body .= ", tel: ";
          $Body .= $tel;
          $Body .= ".";
          $Body .= "\n";
          $Body .= ", land: ";
          $Body .= $country;
          $Body .= ".";
          $Body .= "\n";
          $Body .= "Bedrijf: ";
          $Body .= $company;
          $Body .= ".";
          $Body .= "\n";
          $Body .= "Keuze contact: ";
          $Body .= $method;
          $Body .= ".";
          $Body .= "\n";
          $Body .= "\n";
          $Body .= $message;

          $txt = '
          <html>
          <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <title></title>
          </head>
              <body>
              <div style="width:600px;border:3px solid #ac9d62;margin: 0 auto;border-radius: 10px;">
              <div style="margin:0 auto;padding:25px 10px 5px;color: #ac9d62;">
              Thank you for your interest in the Craft Your Taste application!
              We have received your message and we will contact you in the way you requested.
              You can be sure that someone from our team will contact you as soon as possible!
              <br />
              Thanks and see you soon!
                  <br />
                  <br />
                  Team Craft Your Taste - by Tastefever
                  <br />
                  <img src="https://craftyourtaste.com/public/about/craftyourrtaste.png" alt="Tastefever logo"
                  style="width: 200px; margin: auto; display: block;" />
                </div

                <div style="margin: 0 auto; text-align: center;background-color: #ac9d62;color: white;padding: 10px 0;">
                <a href="https://www.craftyourtaste.com">www.craftyourtaste.com</a> - contact@craftyourtaste.com
                <br />
                More information:
                  <p style="margin: 0"> Tastefever BV - Waregem, Belgium - <a href="https://www.tastefever.com">www.craftyourtaste.com</a>  </p>
                  </div>
                </div>
              </body>
          </html>
';

          $emailtxt = "More info by email";
        
          $mailSubject = "Contact Craft Your Taste";
          $mailSubjectSecond = "Craft Your Taste additional info";
        
          $headersC = "MIME-Version: 1.0" . "\r\n";
          $headersC .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
          $headersC .= "From: ";
          $headersC .= "Tastefever";
          $headersC .= " <";
          $headersC .= $mailTo;
          $headersC .= ">" . "\r\n";
          $headersC .= "Reply-To: ";
          $headersC .= $mailTo;
          $headersC .= "\r\n" . "X-Mailer: PHP/" . phpversion();
        
        $headers = "From: " .$mailFrom ;
        
          // Send email 
        if(mail($mailTo, $subject, $Body, $headers)){ 
          if(mail($mailFrom, $mailSubject, $txt, $headersC)){ 
            if($method == "verstuur meer info via email")
            {
              if(mail($mailFrom, $mailSubjectSecond, $emailtxt, $headersC)){
                header("location:index.html");
              }
              else{ 
                echo 'Email sending failed.'; 
               }
            }
            else {
            header("location:index.html"); 
            }
          }
          else{ 
            echo 'Email sending failed.'; 
           }
        }else{ 
         echo 'Email sending failed.'; 
        }
        } else {
                echo '<h2>You are spammer</h2>';
        }
}
?>